 	all: main.cpp myfunc.cpp myfunc.h
	g++ -g -Wall -o myexe main.cpp myfunc.cpp myfunc.h
.PHONY: clean
clean:
	rm -f myexe main.o myfunc.o
